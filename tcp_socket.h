//
// Created by jerry on 2021/4/5.
//

#ifndef C_TLV_DEMO_TCP_SOCKET_H
#define C_TLV_DEMO_TCP_SOCKET_H

#define TCP_SERVER_IP   "192.168.2.142"
#define TCP_SERVER_PORT 8080

/* 需要发送的tcp消息 */
struct TCP_MSG
{
    char msgLength; // 消息长度
    char *msg;      // 消息体
};

/* 通过tcp短链接发送消息 */
int send_message(struct TCP_MSG *tcpMsg);

#endif //C_TLV_DEMO_TCP_SOCKET_H
